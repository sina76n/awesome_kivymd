from kivy.lang.builder import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import NumericProperty , ListProperty
from kivymd.theming import ThemableBehavior
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.metrics import dp

Builder.load_string("""
<AKSpinnerThreeDots>:
    spacing: root.spinner_size
    size_hint: None, None
    size: dp(root.spinner_size)*3 , dp(root.spinner_size)

    Widget:
        canvas:
            Color:
                rgba: root.theme_cls.primary_color    
            Ellipse:
                size:  root._circle_size1
                pos: [ self.x+self.width/2 - root._circle_size1[0]/2  , self.y+self.height/2 - root._circle_size1[1]/2 ]
    Widget:
        canvas:
            Color:
                rgba: root.theme_cls.primary_color    
            Ellipse:
                size:  root._circle_size2
                pos: [ self.x+self.width/2 - root._circle_size2[0]/2  , self.y+self.height/2 - root._circle_size2[1]/2 ]

    Widget:
        canvas:
            Color:
                rgba: root.theme_cls.primary_color    
            Ellipse:
                size:  root._circle_size3
                pos: [ self.x+self.width/2 - root._circle_size3[0]/2  , self.y+self.height/2 - root._circle_size3[1]/2 ]
""")


class AKSpinnerThreeDots(ThemableBehavior , BoxLayout):
    spinner_size = NumericProperty(48) 
    _circle_size1 = ListProperty([0,0])
    _circle_size2 = ListProperty([0,0])
    _circle_size3 = ListProperty([0,0])


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(lambda x: self._update(dp(self.spinner_size) ) )
        
    def _update(self , size ):
        self._circle_size1 = [0,0]
        self._circle_size2 = [0,0]
        self._circle_size3 = [0,0]
        self._start_animate(size)

    def _start_animate(self , size):
        durtion = 0.5
        
        anim1 = Animation(_circle_size1= [size,size], duration=durtion)\
            + Animation(_circle_size1= [0,0] , duration=durtion)\
            + Animation(duration=durtion)\

        anim2 =Animation(_circle_size2= [size,size], duration=durtion)\
            + Animation(_circle_size2= [0,0] , duration=durtion)\
            + Animation(duration=durtion)\

        anim3 =Animation(_circle_size3= [size,size], duration=durtion)\
            + Animation(_circle_size3= [0,0] , duration=durtion)\
            + Animation(duration=durtion)\

        anim1.repeat = True 
        anim2.repeat = True 
        anim3.repeat = True 

        anim1.start(self)
        Clock.schedule_once(lambda dt: anim2.start(self) , durtion)
        Clock.schedule_once(lambda dt: anim3.start(self), durtion*2)
