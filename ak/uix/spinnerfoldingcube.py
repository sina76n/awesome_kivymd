from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty , ListProperty
from kivymd.theming import ThemableBehavior
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.metrics import dp
Builder.load_string("""

<AKSpinnerFoldingCube>:
    size_hint: None, None
    size: dp(root.spinner_size) , dp(root.spinner_size)
    canvas:
        PushMatrix
        Rotate:
            axis: 0,0,1
            angle: 45
            origin: self.center    
        Color:
            rgba: root.theme_cls.primary_color
            a:root._cube1a
        Rectangle:
            size: root._cubeitem1
            pos: [self.x , self.y+ self.height/2 - root._cubeitem1[1]]
        Color:
            rgba: root.theme_cls.primary_color
            a:root._cube2a            
        Rectangle:
            size: root._cubeitem2
            pos: [self.x+ self.width/2-root._cubeitem2[0] , self.y+self.height/2]
        Color:
            rgba: root.theme_cls.primary_color
            a:root._cube3a            
        Rectangle:
            size: root._cubeitem3
            pos: [self.x+ self.width/2, self.y+ self.height/2]
        Color:
            rgba: root.theme_cls.primary_color
            a:root._cube4a            
        Rectangle:
            size: root._cubeitem4
            pos: [self.x+ self.width/2 , self.y ] 
        PopMatrix    
                    
""")


class AKSpinnerFoldingCube(ThemableBehavior,Widget):
    spinner_size = NumericProperty(48) 
    
    _cubeitem1 = ListProperty([24,24])
    _cubeitem2 = ListProperty([24,24])
    _cubeitem3 = ListProperty([24,24])
    _cubeitem4 = ListProperty([24,24])
    _cube1a = NumericProperty(1)
    _cube2a = NumericProperty(1)
    _cube3a = NumericProperty(1)
    _cube4a = NumericProperty(1)
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.turn = False 
        Clock.schedule_once(lambda x: self._update(dp(self.spinner_size)))

    def _start_animate(self , size):
        speed = 0.3
        anim = 'out_cubic'
        cube_fold = Animation(cubeitem1 = [size ,0] , _cube1a=0 , duration = speed, t=anim)\
            + Animation(_cubeitem2 = [0 ,size] , _cube2a=0 , duration = speed, t=anim)\
            + Animation(_cubeitem3 = [size ,0], _cube3a=0 , duration = speed, t=anim)\
            + Animation(_cubeitem4 = [0 ,size], _cube4a=0 , duration = speed, t=anim)\
            + Animation(_cubeitem4 = [size ,size], _cube4a=1 , duration = speed, t=anim)\
            + Animation(_cubeitem3 = [size ,size], _cube3a=1 , duration = speed, t=anim)\
            + Animation(_cubeitem2 = [size ,size] , _cube2a=1 , duration = speed, t=anim)\
            + Animation(_cubeitem1 = [size ,size] , _cube1a=1 , duration = speed, t=anim)\

        cube_fold.repeat = True
        cube_fold.start(self)

    def _update(self,size):
        cube = [size/2 , size/2]
        self.cubeitem1 = cube
        self.cubeitem2 = cube
        self.cubeitem3 = cube
        self.cubeitem4 = cube 
        self._start_animate(cube[0])
