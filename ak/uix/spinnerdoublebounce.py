from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty , ListProperty
from kivymd.theming import ThemableBehavior
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.metrics import dp

Builder.load_string("""
<AKSpinnerDoubleBounce>:
    size_hint: None, None
    size: dp(root.spinner_size) , dp(root.spinner_size)
    canvas:
        Color:
            rgba: root.theme_cls.primary_color    
        Ellipse:
            size:  root._circle_size1
            pos: [ self.x+self.width/2 - root._circle_size1[0]/2  , self.y+self.height/2 - root._circle_size1[1]/2 ]
        
        Color:
            rgba: root.theme_cls.primary_light
        Ellipse:
            size:  root._circle_size2
            pos: [ self.x+self.width/2 - root._circle_size2[0]/2  , self.y+self.height/2 - root._circle_size2[1]/2 ]
                    
""")


class AKSpinnerDoubleBounce(ThemableBehavior , Widget):
    spinner_size = NumericProperty(48)
    _circle_size1 = ListProperty([48,48])
    _circle_size2 = ListProperty([48,48])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(lambda x: self._update(dp(self.spinner_size) ) )
        
    def _update(self , size ):
        self._circle_size1 = [size,size]
        self._circle_size2 = [0,0]
        self._start_animate(size)

    def _start_animate(self , size):
        durtion = 0.5
        
        anim = Animation(_circle_size1 = [size/2, size/2] , t='in_quad' , duration=durtion)\
            + Animation(_circle_size1 = [size, size], t='out_quad', duration=durtion)

        anim2 = Animation(_circle_size2 = [size/2, size/2] , t='in_quad', duration=durtion)\
            + Animation(_circle_size2 = [0, 0], t='out_quad', duration=durtion)

        anim.repeat= True
        anim2.repeat= True

        anim.start(self)
        anim2.start(self)


