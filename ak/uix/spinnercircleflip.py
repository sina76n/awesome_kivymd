from kivy.lang.builder import Builder
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty , ListProperty
from kivymd.theming import ThemableBehavior
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.metrics import dp
Builder.load_string("""

<AKSpinnerCircleFlip>:
    size_hint: None, None
    size: dp(root.spinner_size) , dp(root.spinner_size)
    canvas:
        Color:
            rgba: root.theme_cls.primary_color
        Ellipse:
            size:  root._circle_size
            pos: [ self.x+self.width/2 - root._circle_size[0]/2  , self.y+self.height/2 - root._circle_size[1]/2 ]
          
""")


class AKSpinnerCircleFlip(ThemableBehavior,Widget):
    spinner_size = NumericProperty(48) 
    _circle_size = ListProperty([48,48])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Clock.schedule_once(lambda x: self._update(dp(self.spinner_size)) )
        
    def _start_animate(self , size):
        speed = 0.3
        anim = 'out_back'
        flip_v = Animation(_circle_size = [size[0] , 0], duration = speed  , t=anim  ) \
            + Animation(_circle_size = [size[0] , size[1]], duration = speed , t=anim  )\
            + Animation(_circle_size = [0 , size[1]], duration = speed , t=anim  )\
            + Animation(_circle_size = [size[0] , size[1]], duration = speed , t=anim  )\

        flip_v.repeat = True
        flip_v.start(self)

    def _update(self , sizee):
        self._circle_size = [sizee, sizee]
        self._start_animate(self._circle_size)

