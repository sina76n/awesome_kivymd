
from distutils.core import setup
import ak

requirements_path='requirements.txt'

def parse_requirements(filename):
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]

install_reqs = parse_requirements(requirements_path)
setup(name='Awesome_KivyMD',
      version=str(ak.__version__),
      description=ak.__description__,
      author=ak.__author__,
      author_email=ak.__email__,
      license='MIT',
      install_requires=parse_requirements(requirements_path) ,
      packages=['ak'],
      package_dir={
          'ak': 'ak',
      },
      package_data={
          'ak': [
              'uix\*.py',
          ]
      }
      )