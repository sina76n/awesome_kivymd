# Awesome KivyMD

Awesome KivyMD is a package containing customized and non-material widgets for KivyMD.

![alt text](http://url/to/img.png)

## Installation

There are two ways for installing this package:

1- Copy the ```ak``` folder and paste it into your project's directory, then import every module you want. Enjoy!

2- Use the package manager [pip](https://pip.pypa.io/en/stable/) to install package:

```bash
pip install git clone https://sina76n@bitbucket.org/sina76n/awesome_kivymd.git
```

- In this case you must add the following to your ```buildozer.spec```
```
requirements =  kivy,kivymd, git+https://sina76n@bitbucket.org/sina76n/awesome_kivymd.git
```

## Usage

You can find usage examples in the demo app
 
## Contributing


## License
[MIT](https://choosealicense.com/licenses/mit/)