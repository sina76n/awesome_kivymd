from kivymd.app import MDApp
from kivy.lang.builder import Builder
from ak.uix.bottomnavigation import AKBottomNavigation
from ak.uix.spinnerdoublebounce import AKSpinnerDoubleBounce
from ak.uix.spinnerfoldingcube import AKSpinnerFoldingCube
from ak.uix.spinnercircleflip import AKSpinnerCircleFlip
from ak.uix.spinnerthreedots import AKSpinnerThreeDots
from ak.uix.labelloader import AKLabelLoader
from ak.uix.imageloader import AKImageLoader

kv = """
<MyMDLabel@AKLabelLoader>
    theme_text_color: 'Primary'
    halign: 'left'

BoxLayout:
    orientation: 'vertical'
    MDToolbar: 
        title: 'Data loading'

    ScrollView
        MDBoxLayout:
            padding: dp(25)
            spacing: dp(5)        
            orientation: 'vertical'
            adaptive_height: True

            AKSpinnerDoubleBounce:

            MDCard:
                padding: "8dp"
                size_hint: None, None
                size: dp(280), dp(150)
                pos_hint: {"center_x": .5, "center_y": .5}

                BoxLayout:
                    FloatLayout:
                        size_hint_x: 0.3
                        AKImageLoader:
                            id: i1
                            size_hint: None,None 
                            size: dp(50),dp(50)
                            pos_hint: {'center_x': .5 , 'center_y': .5}
                            source: ''
                    
                    BoxLayout:
                        orientation: 'vertical'
                        size_hint_x: 0.7
                        padding: dp(10)
                        spacing: dp(10)
                        MyMDLabel:
                            id:l1
                            size_hint_y: None 
                            height: dp(20)
                            text: ''
                        MDSeparator:
                        MyMDLabel:
                            id:l2
                            size_hint_y: None 
                            height: dp(20)
                            font_style: 'Subtitle2'
                            text: ''
                        MDSeparator:
                        MyMDLabel:
                            id:l3
                            size_hint_y: None 
                            font_style: 'Subtitle2'
                            height: dp(20)
                            text: ''
                        Widget:
            MDCard:
                padding: "8dp"
                size_hint: None, None
                size: dp(280), dp(150)
                pos_hint: {"center_x": .5, "center_y": .5}

                BoxLayout:
                    FloatLayout:
                        size_hint_x: 0.3
                        AKImageLoader:
                            size_hint: None,None 
                            size: dp(50),dp(50)
                            pos_hint: {'center_x': .5 , 'center_y': .5}
                            source: i1.source
                    
                    BoxLayout:
                        orientation: 'vertical'
                        size_hint_x: 0.7
                        padding: dp(10)
                        spacing: dp(10)
                        MyMDLabel:
                            size_hint_y: None 
                            height: dp(20)
                            text:  l1.text
                        MDSeparator:
                        MyMDLabel:
                            size_hint_y: None 
                            height: dp(20)
                            font_style: 'Subtitle2'
                            text:  l2.text
                        MDSeparator:
                        MyMDLabel:
                            size_hint_y: None 
                            height: dp(20)
                            font_style: 'Subtitle2'
                            text: l3.text
                        Widget:                        
    MDBoxLayout:
        adaptive_height: True
        padding: dp(10)
        spacing: dp(10)
        MDRaisedButton:
            text: 'Add Content'
            on_release:
                l1.text= 'Some User'
                l2.text= 'Balance: 14152$'
                l3.text= 'This is a test'
                i1.source= 'image.png'
        MDRaisedButton:
            text: 'Remove Content'
            on_release:
                l1.text= ''
                l2.text= ''
                l3.text= ''
                i1.source= ''
        MDRaisedButton:
            text: 'Add to nav'
            on_release: app.add_item()

    AKBottomNavigation:
        id: bottom
        items: app.items


                                        
"""

class MyApp(MDApp):

    items=[
        {'icon': 'menu' , 'text': 'menu' , 'on_release': lambda x: print('ee12')},
        {'icon': 'android' , 'text': 'android' , 'on_release': lambda x: print('cc')},
        {'icon': 'floppy', 'text': 'floppy' , 'on_release': lambda x: print('aascac')},
        {'icon': 'account', 'text': 'account' , 'on_release': lambda x: print('a231')},      
    ]

    def add_item(self):
        items = self.kv.ids.bottom.items
        items.append({'icon': 'menu' , 'text': 'menu' , 'on_release': lambda x: print('ee12')})

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.theme_cls.primary_palette = 'Teal'
        self.theme_cls.accent_palette = 'Teal'
        self.theme_cls.theme_style = 'Light'

    def build(self):
        self.kv = Builder.load_string(kv)
        return self.kv

MyApp().run()